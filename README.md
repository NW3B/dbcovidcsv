# CovidCSV

- [ ] Application qui récupère des données concernant le Covid19 en Csv sur le site data.gouv
Pour alimenter une BDD (mise à jour possible)

- [ ] Info :
* nombre de ligne à rajouter pour mettre la base de données à jour
* dernière date en Base de données
* dernière date sur fichier csv 
``php bin/console covid:post info``

- [ ] Actualiser la base de donnée :
``php bin/console post:covid run``


- [ ] doc : https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/
