<?php

namespace App\Entity;

use App\Repository\DataRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DataRepository::class)
 */
class Data
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $region;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $txDC7jour;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $txHO7jour;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $txSC7jour;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $txHOprev;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $txSCprev;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRegion(): ?int
    {
        return $this->region;
    }

    public function setRegion(int $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTxDC7jour(): ?float
    {
        return $this->txDC7jour;
    }

    public function setTxDC7jour(?float $txDC7jour): self
    {
        $this->txDC7jour = $txDC7jour;

        return $this;
    }

    public function getTxHO7jour(): ?float
    {
        return $this->txHO7jour;
    }

    public function setTxHO7jour(?float $txHO7jour): self
    {
        $this->txHO7jour = $txHO7jour;

        return $this;
    }

    public function getTxSC7jour(): ?float
    {
        return $this->txSC7jour;
    }

    public function setTxSC7jour(?float $txSC7jour): self
    {
        $this->txSC7jour = $txSC7jour;

        return $this;
    }

    public function getTxHOprev(): ?float
    {
        return $this->txHOprev;
    }

    public function setTxHOprev(?float $txHOprev): self
    {
        $this->txHOprev = $txHOprev;

        return $this;
    }

    public function getTxSCprev(): ?float
    {
        return $this->txSCprev;
    }

    public function setTxSCprev(?float $txSCprev): self
    {
        $this->txSCprev = $txSCprev;

        return $this;
    }
}
