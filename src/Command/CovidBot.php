<?php

namespace App\Command;

use App\Controller\DataController;
use App\Services\GetDataService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CovidBot extends Command
{
    private $getData;
    private $getController;
    protected static $defaultName = 'post:covid';

    public function __construct(GetDataService $getData, DataController $getController)
    {
        parent::__construct();
        $this->getData = $getData;
        $this->getController = $getController;
    }

    protected function configure()
    {
        $this
        // ...
        ->setDescription('Actualiser la base de données avec données CSV')
        ->addArgument('info', InputArgument::OPTIONAL, 'Info avant post.')
        ->addArgument('run', InputArgument::OPTIONAL, 'Poster dans BD.')
    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) :int
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            '===================================',
            'Actualisation de la Base de données',
            '===================================',
            '             COMMANDES :           ',
            ' $ php bin/console post:covid info',
            ' $ php bin/console post:covid run',
            '===================================',
        ]);

        $info = $input->getArgument('info');
        $run = $input->getArgument('info');

        if($info == "info")
        {
            $output->writeln([
                'INFO',
                '=====',
            ]);
            $content = $this->getData->csvTabRow();
            $nbPost = $this->getController->info($content); 
            $output->writeln("\n=== " . $nbPost . " ligne(s) à ajoutée(s) ! ===");
            $output->writeln("\n=== Commande => php bin/console post:covid run ===\n");
        }

        if($run == "run")
        {
            $output->writeln([
                'POST',
                '=====',
                '>',
            ]);
            $content = $this->getData->csvTabRow();
            $nbPost = $this->getController->post($content); 
            $output->writeln("=== Actualisation Terminée ===");
            $output->writeln("=== " . $nbPost . " ligne(s) ajoutée(s)  ===\n");
        }
        
        return Command::SUCCESS;
    }
}