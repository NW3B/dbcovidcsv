<?php

namespace App\Services;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class GetDataService
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;        
    }

    function csvTabRow(): array
    {
        $fname = 'https://www.data.gouv.fr/fr/datasets/r/46f145d4-9607-45a0-bc3c-86241136ca24';
        
        $csv = file_get_contents($fname);
        $ligne = str_getcsv($csv,"\n");

        // Supprimer le premier élément du tableau (les clés)
        $supp = array_shift($ligne);

        return $ligne;

    }           
}


