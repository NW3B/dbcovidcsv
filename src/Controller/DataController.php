<?php

namespace App\Controller;

use App\Repository\DataRepository;
use App\Entity\Data;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;


class DataController extends AbstractController
{
    
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->entityManager = $doctrine->getManager();        
        $this->repository = $doctrine->getRepository(Data::class);
        
    }

    
    public function post($ligne): int
    {         
        $dateLastDb = $this->repository->findLastDate();

        $comptExt = 0;
        $comptInt = 0;

         foreach($ligne as $valeur)
         {
            $value = explode(";", $valeur);

             if( new Datetime($value[1]) > $dateLastDb )
             {
                $data = new Data();
                $data->setRegion($value[0]);
                $data->setDate(new Datetime($value[1]));
                $data->setTxDC7jour($value[2]);
                $data->setTxHO7jour($value[3]);
                $data->setTxSC7jour($value[4]);
                $data->setTxHOprev($value[5]);
                $data->setTxSCprev($value[6]);

                $this->entityManager->persist($data);
                $this->entityManager->flush();
                $comptInt++;
             }
             $comptExt++;
         }
         return $comptInt;
    }

    public function info($ligne)
    {
        $comptExt = 0;
        $comptInt = 0;

        $dateLastDb = $this->repository->findLastDate();
        

        print_r("> Dernière Date dans Base de données = " . $dateLastDb->format("d/m/Y") . "\n");
    
        $lastKey = array_key_last($ligne);
        $lastRow = explode(";", $ligne[$lastKey]);
        $lastDate = new DateTime($lastRow[1]); 
        print_r("> Dernière Date fichier CSV = " . $lastDate->format("d/m/Y") . "\n");
        
        foreach($ligne as $valeur)
        {
        $value = explode(";", $valeur);

            if( new Datetime($value[1]) > $dateLastDb )
            {
                $comptInt++;
            }

            $comptExt++;
        }

        return $comptInt;
    }
}
