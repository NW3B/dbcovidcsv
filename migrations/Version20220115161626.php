<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220115161626 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Création base de donnée Covid19 par régions';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE data (id INT AUTO_INCREMENT NOT NULL, region INT NOT NULL, date DATETIME NOT NULL, tx_dc7jour DOUBLE PRECISION DEFAULT NULL, tx_ho7jour DOUBLE PRECISION DEFAULT NULL, tx_sc7jour DOUBLE PRECISION DEFAULT NULL, tx_hoprev DOUBLE PRECISION DEFAULT NULL, tx_scprev DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE data');
    }
}
